import os
import socket
host = '192.168.43.37'  # Put your computer's own IP address
port = 13000
buf = 1024
addr = (host, port)
receive = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
receive.bind(addr)
print('Waiting to receive messages...')
while True:
    (data, addr) = receive.recvfrom(buf)
    print('Received message: ', data.decode())
    if data.decode() == 'exit':
        break
receive.close()
os._exit(0)
